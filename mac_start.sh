#!/usr/bin/env bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install zsh curl git wget
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
exec zsh
brew tap caskroom/cask
sdk install java
sdk install kotlin
sdk install kscript
sdk install groovy
sdk install gradle
sdk install springboot
brew install rustup-init tree less grip python sl fswatch node pipenv vim bat autojump jq darksky-weather
brew cask install intellij-idea visual-studio-code slack google-chrome firefox iterm2 docker atom flux